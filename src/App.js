import React, { Component } from 'react';
import ReactDom from 'react-dom';
import listingData from'./component/datalist';
import Test from'./component/test';
import Fun from'./component/fun';
import Sub from'./component/sub';
import Eventbinding from'./component/eventbinding';
import { render } from '@testing-library/react';
import Parent from'./component/parent';
import RenCondition from'./component/renCondition';
import List from'./component/list';
import Form from'./component/form';
import Update from'./component/update';
// import Popupdate from'./component/Popupdate';
import Validate from'./component/validation';
import Search from'./component/search';
import registerbus from'./component/registerBus';
import Buslist from'./component/buslist';
import addStops from'./component/addstops';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Route, Link, Switch } from "react-router-dom";

class App extends Component {
  

  render(){
  return (
<BrowserRouter>
    <div>
{/* <h2>hello world</h2> */}
    {/* <Test />
    <Fun name='Ayana'  fatherName='David'>
<p>hi i am child method</p>

    </Fun>
    <Fun name='Trump' fatherName='Rolex'>
    <button>Click me</button>
    
    </Fun>
    <Fun  name='David'fatherName='Camron'/>
    <Fun  name='Alan'fatherName='Walker'/>
   
    <Eventbinding />
    <Parent />

    <RenCondition /> */}

    {/* <List /> */}

<Switch>
    <Route path='/' exact component={Form} />
    <Route path='/list' component={listingData} />
    <Route path='/Login' component={Sub} />
       {/* <Route path='/editing' component={Popupdate} /> */}
    <Route path='/update' component={Update} />
    <Route path='/buslist' component={Buslist} />
   <Route path='/validate' component={Validate}/>
   <Route path='/buslist' component={Buslist}/>
   <Route path='/register' component={registerbus}/>
   <Route path='/stop' component={addStops}/>
   <Route path='/search' component={Search}/>
    </Switch>
    <ToastContainer autoClose={2000} />
    </div>
    </BrowserRouter>

    



  )}
}

export default App;
