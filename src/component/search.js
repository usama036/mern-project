import React, { Component } from 'react'
import DatePicker from "react-datepicker";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios';
import makeAnimated from 'react-select/animated';
var moment = require('moment');
export class Search extends Component {

constructor(){
    super()
    this.state={

        busfrom:'',
        busto:'',
        date:'',
        array:[], 
        startDate: new Date(),
    }
}
busfrom=(event)=>{
    this.setState({
        busfrom:event.target.value
    })
}
    busto=(event)=>{
        this.setState({
            busto:event.target.value
        })
    }


handleChange = date => {
    this.setState({
      startDate: date
    
    });
    
  };

onsubmit=(e)=>{
   let data={startDate:new Date(this.state.startDate).toISOString().slice(0,10),busFrom:this.state.busfrom,text:this.state.busto}
   console.log(data)
   axios.post('http://localhost:5000/api/search',data).then(res=>{

    console.log(res)

  if (res.data[0].status === true) {
      this.setState({
        array:res.data[0].data 
      })

    
    toast.success(res.data[0].message);

}else if(res.data[0].status === false){

  toast.error(res.data[0].message)}

 
})



}


    render() {
        return (
            <div>
              
<div class="container">
  <h2>Search Bus Here</h2>
  <p>Welcome to Emirates Pakistan</p> 

  <div class="form-inline" >
  <div class="form-group">
    <label for="email">BusFrom:</label>
    <input class="form-control" value={this.state.busfrom}  onChange={this.busfrom} />
  </div>
  <div class="form-group">
    <label for="pwd">BusTo:</label>
    <input  class="form-control"   value={this.state.busto}  onChange={this.busto} />
  </div>
  <label for="email">Select date:</label>
  <DatePicker className="form-control" 
        selected={this.state.startDate}
        onChange={this.handleChange}
      />
 
    

  <button type="submit" class="btn btn-outline-success"  onClick={this.onsubmit}>Submit</button>
</div>
<br></br>
  <table class="table">
    <thead>
      <tr>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Email</th>
      </tr>
    </thead>
    <tbody>
    {this.state.array.map((data,index)=>
     
     <tr key={data._id}>
      <th scope="row">{index +1}</th>
      <td>{data.busnumber} </td>
     <td>{data.busFrom} </td>
      <td>{data.busTo}</td>
      <td>{data.fare}</td>
      <td>{data.select}</td>
      <td>{data.select2}</td>
      
     
   
     {data.status=='drop'?<td style={{backgroundColor:'red'}} >{data.status}</td>:<td  >{data.status}</td>}
     



      
     
     
     
     {data.stops.map(aa=><td>{aa.value}</td>)} 
    </tr> )}
    
    </tbody>
  </table>
</div>


            </div>
        )
    }
}

export default Search
