import React, { Component } from 'react'

import '../App.css'
import pic from './aa.jpg'
import "react-datepicker/dist/react-datepicker.css";


import PropTypes from 'prop-types'
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Link } from "react-router-dom";
import Datalist from './datalist'
import DatePicker from "react-datepicker";
// import { colourOptions } from './multioption';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';



const animatedComponents = makeAnimated();


const colourOptions = [
  { value: 'Okara', label: 'Okara', color: '#00B8D9', },
  { value: 'Sahiwal', label: 'Sahiwal', color: '#0052CC', },
  { value: 'Khanewal', label: 'Khanewal', color: '#5243AA' },

];

const initialState={
  busFrom:'Lahore',
  busTo:'Multan',
  startDate: new Date(),
   select:'12:00',
  select2:'01:00',
 fare:"",
  busnumber:'',
  phone:'',
  city:'',
  zip:'',
  status:'active',
  stop:[],

 
  emailError:'',
  passwordError:'',

  
}
 class BusRegister extends Component {
   constructor(props) {
       super(props)
   
       this.state = initialState
   }




   emailHandlechange=(event)=>{
this.setState({
  busFrom:event.target.value
})

   }

   passwordHandlechange=(event)=>{
    this.setState({
      busTo:event.target.value
    })
    
    
       }

       statuseventHandler=event=>{
        this.setState({
          status:event.target.value
        })
        
        
           }

       Fareeventhandle=(event)=>{
        this.setState({
          fare:event.target.value
        })
        
        
           }

       selectHandlechange=  event  =>{

        this.setState({
            select:event.target.value
        })
       }

       selectHandlechange2=  event  =>{

        this.setState({
            select2:event.target.value
        })
       }
//=======================================




// radiohandlechaneg=(event)=>{
// let data=this.state.radio
//  this.setState({
// radio:!data
// })
// console.log(this.state.radio)

//  }
    

busnumberevent=(event)=>{
  this.setState({
    busnumber:event.target.value
  })
  
     }

      


      addressHandlechange=(event)=>{
        this.setState({
            address:event.target.value
        })
        
           }

           
           phoneHandlechange=(event)=>{
            this.setState({
                phone:event.target.value
            })
            
               }
               handleChange = date => {
                this.setState({
                  startDate: date
                });
              };
               
               cityHandlechange=(event)=>{
                this.setState({
                    city:event.target.value
                })
                
                   }
                  zipHandlechange=(event)=>{
                    this.setState({
                        zip:event.target.value
                    })
                    
                       }

                       stopselectEvent=(event)=>{  
                         this.setState({
                        stop:event
                    })
                    }
                //=======================================


                valid=()=>{
         if(this.state.busFrom==this.state.busTo){
this.setState({
  emailError:"You cant select same city"
})
             }
             else{
                   return  true
                   }
               }


           submitevent=event=>{  
            event.preventDefault();

            if(this.valid()){
              const data=this.state

            console.log(data)
         

axios.post('http://localhost:5000/api/register',data).then(res=>{



  if (res.data[0].status === true) {
 
    
    toast.success("data save");
    this.props.history.push('/buslist')
}else if(res.data[0].status === false){

  toast.error(res.data[0].message)}

 
})






            }
}


    render() {
        return (

<div>
<h1 className="App" style={{backgroundColor:'yellow'}}>Bus Registration form </h1>


            
 <form onSubmit={this.submitevent} className="container">
      
  <div class="form-row">
    <div class="form-group col-md-4">
      <label for="inputEmail4">Bus Number</label>
      <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Bus Number" value={this.state.busnumber} onChange={this.busnumberevent} />
     
    </div>
    <div class="form-row">
    <div class="form-group col-md-4">
      <label for="inputEmail4">Fare</label>
      <input type="text" class="form-control" id="inputEmail4" placeholder="Enter Fare" value={this.state.fare} onChange={this.Fareeventhandle} />
     
    </div>

    <div class="form-group col-md-4">
      <label for="inputState">Bus From</label>
      <select id="inputState" class="form-control" value={this.state.busFrom} onChange={this.emailHandlechange}>
        <option selected>Lahore</option>
        <option>Multan</option>
        <option>Rawalpindi</option>
        <option>Sadiqabad</option>
      </select>
    </div>
    <div class="form-group col-md-4">
      <label for="inputState">Bus to</label>
      <select id="inputState" class="form-control" value={this.state.busTo} onChange={this.passwordHandlechange}>
        <option selected>Lahore</option>
        <option>Multan</option>
        <option>Rawalpindi</option>
        <option>Sadiqabad</option>
      </select>
      <p class="help-block" style={{color:"red"}}>{this.state.emailError} </p>

    </div>

    <div class="form-group col-md-4">
      <label for="inputState">Bus Type</label>
      <select id="inputState" class="form-control" value={this.state.select} onChange={this.selectHandlechange}>
        <option selected>Luxury</option>
        <option>Executive</option>
        <option>Business CLass</option>
        <option>Premium Gold</option>
      </select>
    </div>

    <div class="form-group col-md-4">
      <label for="inputState">status</label>
      <select id="inputState" class="form-control" value={this.state.status} onChange={this.statuseventHandler}>
        <option selected >active</option>
        <option >drop</option>
       
      </select>
    </div>




 

    {/* <div class="checkbox-inline"><input  type="checkbox" value={this.state.radio} onChange={this.radiohandlechaneg}/></div>
    
    
    
    <div class="form-group col-md-4"  className={this.state.radio ? 'd-none' : 'show'} >
      <label for="inputState">Stops</label>
      <select id="inputState"  class="form-control" multiple value={this.state.select} onChange={this.selectHandlechange}>
        <option selected>Khanewal</option>
        <option>Shaiwal</option>
        <option>Okara</option>
      </select>
    </div> */}


    <div class="form-group col-md-4">
      <label for="inputState">Departure Time</label>
      <select id="inputState" class="form-control" value={this.state.select} onChange={this.selectHandlechange}>
        <option selected>12:00</option>
        <option>13:00</option>
        <option>14:00</option>
        <option>15:00</option>
        <option>16:00</option>
        <option>17:00</option>
        <option>18:00</option>
        <option>19:00</option>
        <option>20:00</option>
        <option>21:00</option>
        <option>22:00</option>
        <option>23:00</option>
        <option>24:00</option>
        <option>01:00</option>
        <option>02:00</option>
        <option>03:00</option>
        <option>05:00</option>
        <option>06:00</option>
        <option>07:00</option>
        <option>08:00</option>
        <option>09:00</option>
        <option>10:00</option>
        <option>11:00</option>
        


      </select>
    </div>




    <div className="form-group col-md-4">
      <label for="inputState">Arrival time Time</label>
      <select id="inputState" class="form-control" value={this.state.select2} onChange={this.selectHandlechange2}>
        <option selected>12:00</option>
        <option>13:00</option>
        <option>14:00</option>
        <option>15:00</option>
        <option>16:00</option>
        <option>17:00</option>
        <option>18:00</option>
        <option>19:00</option>
        <option>20:00</option>
        <option>21:00</option>
        <option>22:00</option>
        <option>23:00</option>
        <option>24:00</option>
        <option>01:00</option>
        <option>02:00</option>
        <option>03:00</option>
        <option>05:00</option>
        <option>06:00</option>
        <option>07:00</option>
        <option>08:00</option>
        <option>09:00</option>
        <option>10:00</option>
        <option>11:00</option>
        


      </select>



    </div >


   <Select style={{width: 100}}
    
      isMulti
      options={colourOptions}
   value={this.state.stop}
   onChange={this.stopselectEvent}
    /> 


    <div className="form-group col-md-4" >
    <label for="inputState">Date</label>

 <DatePicker 
        selected={this.state.startDate}
        onChange={this.handleChange}
      />
</div>
  </div>
  </div>




 


 


 
 
  <button type="submit" class="btn btn-primary">Submit</button>

        
 
        </form>
    
        </div>
        )
    }
}


export default BusRegister

