import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './App.css'
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Link } from "react-router-dom";
import Datalist from './datalist'

 class Form extends Component {
   constructor(props) {
       super(props)
   
       this.state = {
           _id:'',
            email:'',
            password:'',
            select:'Angular',
            address:'',
            phone:'',
            city:'',
            zip:'',
       }
   }


   componentDidMount(){

  
const id =this.props.location.state

    axios.get('http://localhost:5000/api/appointments/'+id).then(response=>{
   const res=response.data[0].data
   this.setState({_id:res._id})
    this.setState({email:res.email})
    this.setState({password:res.password})
    this.setState({address:res.address})
    this.setState({phone:res.phone})
    this.setState({city:res.city})
    this.setState({select:res.select})
    this.setState({zip:res.zip})

    })
    
}
emailHandlechange=(event)=>{
    this.setState({
        email:event.target.value
    })
    
       }
    
       passwordHandlechange=(event)=>{
        this.setState({
            password:event.target.value
        })
        
           }
    
           selectHandlechange=  event  =>{
    
            this.setState({
                select:event.target.value
            })
           }
    //=======================================
    
          addressHandlechange=(event)=>{
            this.setState({
                address:event.target.value
            })
            
               }
    
               
               phoneHandlechange=(event)=>{
                this.setState({
                    phone:event.target.value
                })
                
                   }
    
                   
                   cityHandlechange=(event)=>{
                    this.setState({
                        city:event.target.value
                    })
                    
                       }
                      zipHandlechange=(event)=>{
                        this.setState({
                            zip:event.target.value
                        })
                        
                           }


  
                //=======================================



           submitevent=event=>{         
const data=this.state
console.log(data._id)
axios.post('http://localhost:5000/api/appointments/'+data._id,data).then(res=>{
console.log(res.data);
  if (res.data[0].status === true) {
 
    
    toast.success("data Update");
    this.props.history.push('/list')
}
 
})



event.preventDefault();
}


    render() {
        return (

<div>


<nav class="navbar navbar-expand-lg primary-color">


  <Link to="/Login"> Login </Link>


  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  
  <div class="collapse navbar-collapse" id="basicExampleNav">

    
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" ><Link to="/list"> Home </Link>
          <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Features</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Pricing</a>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false">Dropdown</a>
        <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>

    </ul>
  
   
   
  </div>
 

</nav>
            
 <form onSubmit={this.submitevent}>
      
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">Email</label>
      <input type="email" class="form-control" id="inputEmail4" placeholder="Email" value={this.state.email} onChange={this.emailHandlechange} />
    </div>


    <div class="form-group col-md-6">
      <label for="inputPassword4">Password</label>
      <input type="password" class="form-control" id="inputPassword4" placeholder="Password"  value={this.state.password} onChange={this.passwordHandlechange}/>
    </div>
  </div>


  <div class="form-group">
    <label for="inputAddress">Address</label>
    <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St" value={this.state.address} onChange={this.addressHandlechange} />
  </div>


  <div class="form-group">
    <label for="inputAddress2">Phone</label>
    <input type="text" class="form-control" id="inputAddress2" placeholder="Enter your Contact Number"  value={this.state.phone} onChange={this.phoneHandlechange}/>
  </div>


  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputCity">City</label>
      <input type="text" class="form-control" id="inputCity" value={this.state.city} onChange={this.cityHandlechange} />
    </div>


    <div class="form-group col-md-4">
      <label for="inputState">Expertise</label>
      <select id="inputState" class="form-control" value={this.state.select} onChange={this.selectHandlechange}>
        <option selected>React</option>
        <option>Vue</option>
        <option>Angular</option>
      </select>
    </div>


    <div class="form-group col-md-2">
      <label for="inputZip">Zip</label>
      <input type="text" class="form-control" id="inputZip" value={this.state.zip} onChange={this.zipHandlechange}/>
    </div>
  </div>
  <input type="hidden" value={this.state._id} />
 
  <button type="submit" class="btn btn-primary">Sign in</button>

        
 
        </form>
        </div>
        
        )
    }
}

export default Form
