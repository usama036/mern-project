import React, { Component}from 'react';
import { Link } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import '../App.css'
import axios from 'axios';



 class Sub extends Component {

  constructor(props) {
    super(props)

    this.state = {
         email:'',
         password:'',
    }
}

emailChangeEvent=(event)=>{
  this.setState({
    email:event.target.value
  })
}

passwordChangeEvent=(event)=>{
  this.setState({
    password:event.target.value
  })
}

Submit=(event)=>{
  const data=this.state;
axios.post('http://localhost:5000/api/login',data).then(res=>{

  
    if (res.data.status === true) {
       
        
        toast.success("user found");
        this.props.history.push('/list')
    }else if(res.data[0].status === false){
        console.log('test');
        toast.error(res.data[0].message)}
})
event.preventDefault()
}


  
  render() {
    return (
      <div>
          
      <nav className="navbar navbar-expand-lg navbar-light navbar-laravel">
      <div className="container">
        
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
          </button>
  
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav ml-auto">
                  <li className="nav-item">
                      <a className="nav-link" href="#"></a>
                  </li>
                 
              </ul>
  
          </div>
      </div>
  </nav>
  
  <main className="login-form">
      <div className="cotainer">
          <div className="row justify-content-center">
              <div className="col-md-8">
                  <div className="card">
                      <div className="card-header"><Link to="/"> Login </Link></div>
                      <div className="card-body">
                          <form onSubmit={this.Submit}>
                              <div className="form-group row">
                                  <label for="email_address" className="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                                  <div className="col-md-6">
                                      <input type="text" id="email_address" className="form-control"  value={this.state.email} onChange={this.emailChangeEvent} name="email-address" required autoFocus />
                                  </div>
                              </div>
  
                              <div className="form-group row">
                                  <label for="password" className="col-md-4 col-form-label text-md-right">Password</label>
                                  <div className="col-md-6">
                                      <input type="password" value={this.state.password} onChange={this.passwordChangeEvent} id="password" className="form-control" name="password" required />
                                  </div>
                              </div>
  
                              <div className="form-group row">
                                  <div className="col-md-6 offset-md-4">
                                      <div className="checkbox">
                                          <label>
                                              <input type="checkbox" name="remember" /> Remember Me
                                          </label>
                                      </div>
                                  </div>
                              </div>
  
                              <div className="col-md-6 offset-md-4">
                                  <button type="submit" className="btn btn-primary" >
                                      Login
                                  </button>
                                  <a href="#" className="btn btn-link">
                                      Forgot Your Password?
                                  </a>
                              </div>
                 
                      </form>
                  </div>
              </div>
          </div>
      </div>
  
      </div>
  </main>
  </div>
    )
  }
}

export default Sub






