import React, { Component } from 'react'
import '../App.css'  
import { toast } from 'react-toastify'

class Validation extends Component {

constructor(props) {
    super(props)

    this.state = {
         username:'',
         email:'',
         password:'',
         password_confirm:'',
         usernameError:'',
         emailError:'',
         passwordError:'',
         password_confirmError:''
        
    }
}


usernamehandlevent=event=>{
    this.setState({
        username:event.target.value
    })
}


valid=()=>{
    if(!this.state.username.includes("@")){
this.setState({
    usernameError:"invlid username"
})

    }else{return true}
}

submitevent=event=>{
    event.preventDefault();
    if(this.valid()){
    console.log(this.state.username)

}

 
}





    render() {
        return (
            <div className="App"> 
              <form class="form-horizontal" onSubmit={this.submitevent} >
              <fieldset>
    <div id="legend">
      <legend class="">Register</legend>
    </div>
    <div class="control-group">
  
      <label class="control-label"  for="username">Username</label>
      <div class="controls">
        <input type="text" id="username" name="username" value={this.state.username} onChange={this.usernamehandlevent} placeholder="" class="input-xlarge" />
        <p class="help-block" style={{color:"red"}}>{this.state.usernameError} </p>
      </div>
    </div>
 
    <div class="control-group">
      
      <label class="control-label" for="email">E-mail</label>
      <div class="controls">
        <input type="text" id="email" name="email" placeholder="" class="input-xlarge" />
        <p class="help-block"></p>
      </div>
    </div>
 
    <div class="control-group">
      <label class="control-label" for="password">Password</label>
      <div class="controls">
        <input type="password" id="password" name="password" placeholder="" class="input-xlarge" />
        <p class="help-block"></p>
      </div>
    </div>
 
    <div class="control-group">
      <label class="control-label"  for="password_confirm">Password (Confirm)</label>
      <div class="controls">
        <input type="password" id="password_confirm" name="password_confirm" placeholder="" class="input-xlarge" />
        <p class="help-block"></p>
      </div>
    </div>
 
    <div class="control-group">
      <div class="controls">
      <button type="submit" class="btn btn-primary">Sign in</button>      </div>
    </div>
    </fieldset>
</form>

            </div>
        )
    }
}

export default Validation
