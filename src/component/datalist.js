import React, { Component } from 'react'
import axios from 'axios';
import '../App.css'
import { Link } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
class Datalist extends Component {
    
constructor(props) {
    super(props)

    this.state = {
         array:[]
    }
    // this.singleRowClicked= this.singleRowClicked.bind(this);
}

componentDidMount(){
    this.getAppt();
}
getAppt(){
    axios.get('http://localhost:5000/api/appointments').then(response=>{
    this.setState({
        array:response.data
    })
 }).catch(error=>{
     console.log(error)
 })
}
deletesingleRowClicked=(id)=>{
axios.delete('http://localhost:5000/api/appointments/'+id).then(response=>{
    if (response.data[0].status === true) {
 
        
        toast.success("record has been deleted");
        // this.props.history.push('/list');
        this.getAppt();
    }


}).catch(error=>{
    console.log('this is eeor')
})
}

updatesingleRowClicked=(id)=>{


         this.props.history.push('/update',id)
}




  render() {
     
        return (
            <div>
                <table className="table">
  <thead className="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Email</th>
      <th scope="col">Expertise</th>
      <th scope="col">Address</th>
      <th scope="col">Phone</th>
      <th scope="col">City</th>
      <th scope="col">ZipCode</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  
  <tbody>
     {this.state.array.map((data,index)=>
     
     <tr key={data._id}>
      <th scope="row">{index +1}</th>
     <td>{data.email} </td>
      <td>{data.select}</td>
      <td>{data.address}</td>
      <td>{data.phone}</td>
      <td>{data.city}</td>
      <td>{data.zip}</td>
      <td><button type="button" className=" btn-danger btn1" onClick={()=>this.deletesingleRowClicked(data._id)}>delete</button> 
      <button type="button" class=" btn-warning btn1"onClick={()=>this.updatesingleRowClicked(data._id)} >Update</button></td>
   
    </tr> )}
    
  </tbody>
</table>
            </div>
)
        }
    }

export default Datalist
