import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './App.css'
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Link } from "react-router-dom";
import Datalist from './datalist'


const initialState={
  email:'',
  password:'',
  select:'Angular',
  address:'',
  phone:'',
  city:'',
  zip:'',
 
 
  emailError:'',
  passwordError:'',

  addressError:'',
  phoneError:'',
  cityError:'',
  zipError:'',
}
 class Form extends Component {
   constructor(props) {
       super(props)
   
       this.state = initialState
   }




   emailHandlechange=(event)=>{
this.setState({
    email:event.target.value
})

   }

   passwordHandlechange=(event)=>{
    this.setState({
        password:event.target.value
    })
    
       }

       selectHandlechange=  event  =>{

        this.setState({
            select:event.target.value
        })
       }
//=======================================

      addressHandlechange=(event)=>{
        this.setState({
            address:event.target.value
        })
        
           }

           
           phoneHandlechange=(event)=>{
            this.setState({
                phone:event.target.value
            })
            
               }

               
               cityHandlechange=(event)=>{
                this.setState({
                    city:event.target.value
                })
                
                   }
                  zipHandlechange=(event)=>{
                    this.setState({
                        zip:event.target.value
                    })
                    
                       }
                //=======================================


                valid=()=>{
                    if(!this.state.email.includes("@")){
                      this.setState({
                        emailError:"invalid email"
                      })
                    } else if(this.state.password.length<6){  
                       this.setState({
                      passwordError:"invalid password"
                    })
                   }
                 else if(!this.state.address){  
                    this.setState({
                   addressError:"please Enter your adress"
                 })
                }
                else if(!this.state.phone.length){  
                  this.setState({
                 phoneError:"Enter correct Number"
               })
              }
              else if(!this.state.city){  
                this.setState({
               cityError:"Enter City Name"
             })
            }
            else if(!this.state.zip){  
              this.setState({
             zipError:"Enter ZipCode"
           })
          }
                   
                   else{
                   return  true
                   }
               
               
               
                  }


           submitevent=event=>{  
            event.preventDefault();
            if(this.valid()){
const data=this.state
axios.post('http://localhost:5000/api/appointments',data).then(res=>{
console.log(res.data);
  if (res.data[0].status === true) {
 
    
    toast.success("data save");
    this.setState(initialState)
    this.props.history.push('/login')
}else if(res.data[0].status === false){

  toast.error(res.data[0].message)}

 
})






}
}


    render() {
        return (

<div>


<nav class="navbar navbar-expand-lg primary-color">


  <Link to="/Login"> Login </Link>


  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  
  <div class="collapse navbar-collapse" id="basicExampleNav">

    
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" ><Link to="/list"> Home </Link>
          <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item">

        <a class="nav-link" href="#"><Link to="/register"> Register </Link></a>
      </li>
      <li class="nav-item">

<a class="nav-link" href="#"><Link to="/search"> SearchBus </Link></a>
</li>
      <li class="nav-item">
        <a class="nav-link" href="#"><Link to="/buslist"> Bus list </Link></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" ></a>
 
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false">Dropdown</a>
        <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>

    </ul>
  
   
   
  </div>
 

</nav>
            
 <form onSubmit={this.submitevent}>
      
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">Email</label>
      <input type="text" class="form-control" id="inputEmail4" placeholder="Email" value={this.state.email} onChange={this.emailHandlechange} />
      <p class="help-block" style={{color:"red"}}>{this.state.emailError} </p>
    </div>


    <div class="form-group col-md-6">
      <label for="inputPassword4">Password</label>
      <input type="password" class="form-control" id="inputPassword4" placeholder="Password"  value={this.state.password} onChange={this.passwordHandlechange}/>
      <p class="help-block" style={{color:"red"}}>{this.state.passwordError} </p>

    </div>
  </div>


  <div class="form-group">
    <label for="inputAddress">Address</label>
    <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St" value={this.state.address} onChange={this.addressHandlechange} />
    <p class="help-block" style={{color:"red"}}>{this.state.addressError} </p>
  </div>


  <div class="form-group">
    <label for="inputAddress2">Phone</label>
    <input type="text" class="form-control" id="inputAddress2" placeholder="Enter your Contact Number"  value={this.state.phone} onChange={this.phoneHandlechange}/>
    <p class="help-block" style={{color:"red"}}>{this.state.phoneError} </p>

  </div>


  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputCity">City</label>
      <input type="text" class="form-control" id="inputCity" value={this.state.city} onChange={this.cityHandlechange} />
      <p class="help-block" style={{color:"red"}}>{this.state.cityError} </p>

    </div>


    <div class="form-group col-md-4">
      <label for="inputState">Expertise</label>
      <select id="inputState" class="form-control" value={this.state.select} onChange={this.selectHandlechange}>
        <option selected>React</option>
        <option>Vue</option>
        <option>Angular</option>
      </select>
    </div>


    <div class="form-group col-md-2">
      <label for="inputZip">Zip</label>
      <input type="text" class="form-control" id="inputZip" value={this.state.zip} onChange={this.zipHandlechange}/>
      <p class="help-block" style={{color:"red"}}>{this.state.zipError} </p>
    </div>
  </div>
 
  <button type="submit" class="btn btn-primary">Sign in</button>

        
 
        </form>
        </div>
        
        )
    }
}

export default Form
