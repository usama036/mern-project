import React, { Component } from 'react'
import axios from 'axios';
import '../App.css'
import Stop from './addstops'
import { Link } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
class Buslist extends Component {
    
constructor(props) {
    super(props)

    this.state = {
         array:[]
    }
    // this.singleRowClicked= this.singleRowClicked.bind(this);
}

componentDidMount(){
    this.getAppt();
}
getAppt(){
    
    axios.get('http://localhost:5000/api/listBus').then(response=>{

    this.setState({
        array:response.data[0].data
    })
console.log("==================================");
// console.log(this.state.array[0].data)
let stop =this.state.array.map(data=>data.stop.map(aa=>aa.value))

console.log(stop)

console.log("==================================");


}).catch(error=>{
     console.log(error)
 })
}

dropsingleRow=(id)=>{
    axios.put('http://localhost:5000/api/status/'+id).then(response=>{
        if (response.data[0].status === true) {
 
        
            toast.success("status upate");}
  this.getAppt();

})

}

deletesingleRowClicked=(id)=>{
    axios.delete('http://localhost:5000/api/deleteBus/'+id).then(response=>{
        if (response.data[0].status === true) {
 
        
            toast.success("record has been deleted");}
  this.getAppt();

})

}
updateSingleRow=id=>{
    axios.put('http://localhost:5000/api/activeStatus/'+id).then(response=>{
        if (response.data[0].status === true) {
 
        
            toast.success(" bus update");}
  this.getAppt();

})
}
addstop=(id)=>{
    this.props.history.push('/stop',id)
}



  render() {
     
        return (
            <div>
                <table className="table">
  <thead className="thead-dark">
  <tr>
      <th scope="col">#</th>
      <th scope="col">busNumber</th>
      <th scope="col">busFrom</th>
      <th scope="col">busTo</th>
      <th scope="col">fare</th>
      <th scope="col">Departure Time</th>
      <th scope="col">Arrival Time</th>
      <th scope="col">Departure Date</th>
      <th scope="col">status</th>
      <th scope="col">action</th>
      <th scope="col">drop</th>
      <th scope="col">active</th>
      <th>stop</th>

  
    </tr>
  </thead>
  
  <tbody>
      
  {this.state.array.map((data,index)=>
     
     <tr key={data._id}>
      <th scope="row">{index +1}</th>
      <td>{data.busnumber} </td>
     <td>{data.busFrom} </td>
      <td>{data.busTo}</td>
      <td>{data.fare}</td>
      <td>{data.select}</td>
      <td>{data.select2}</td>
      
      <td>{ new Date(data.startDate).toISOString().slice(0,10)}</td>
   
     {data.status=='drop'?<td style={{backgroundColor:'red'}} >{data.status}</td>:<td  >{data.status}</td>}
     



      
      <td><button type="button" className=" btn-danger btn1" onClick={()=>this.deletesingleRowClicked(data._id)}>delete</button> </td> 
      <td><button type="button" className=" btn-primary btn1" onClick={()=>this.dropsingleRow(data._id)}>drop</button> </td> 
      <td><button type="button" className=" btn-primary btn1" onClick={()=>this.updateSingleRow(data._id)}>active</button> </td> 
      <td><button type="button" className=" btn-primary btn1" onClick={()=>this.addstop(data._id)}>addstop</button> </td> 
     
     
     
     
      {/* {data.stop.map(aa=><td>{aa.value}</td>)} */}
    </tr> )}
    
  </tbody>
</table>

            </div>
)
        }
    }

export default Buslist
