const mongoose = require('mongoose');

const AppointmentSchema = mongoose.Schema({
              email:{ type:String},
            password:{ type:String},
            select:{ type:String},
            address:{ type:String},
            phone:{ type:String},
            city:{ type:String},
            zip:{ type:String},
});

module.exports = mongoose.model('Appointment', AppointmentSchema);
