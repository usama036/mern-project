const mongoose = require('mongoose');

const busRegister = mongoose.Schema({
              busFrom:{ type:String},
              busnumber:{type:String},
              busTo:{ type:String},
              startDate:{ type:String},
              select:{ type:String},
              select2:{ type:String},
              fare:{type:String},
              status:{type:String},
              stops:[{
                  stop:{type:String},
                  time:{type:String },
                  createdAt:{type:Date,
                default: new Date()}
              }],
         
              stop:{},
});

module.exports = mongoose.model('busRegister', busRegister);